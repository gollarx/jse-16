package ru.t1.shipilov.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showVersion();

    void showAbout();

    void showHelp();

}
